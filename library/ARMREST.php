<?php



class ARMREST {


    /**
     * Iterates over the JS templates directory and returns an iterator of file paths for including in html output.
     *
     * @static
     * @access public
     * @return Iterator(<string>)
     */
    static public function getJsTemplates() {
        $dir  = new RecursiveDirectoryIterator(realpath($_SERVER['DOCUMENT_ROOT'].'/js/tpl/'));
        $iter = new RecursiveIteratorIterator($dir);
        $res  = new RegexIterator($iter, '/^.+\.tpl\.html$/i', RecursiveRegexIterator::GET_MATCH);
        return $res;
    }

}


