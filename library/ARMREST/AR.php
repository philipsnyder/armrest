<?php
/**
 * @author Philip Snyder <philip.r.snyder@gmail.com>
 */

/**
 * Defines parent class to implement active record and toJson.
 *
 */
abstract class ARMREST_AR implements ARMREST_AR_Interface_toJson, JsonSerializable {

    /**
     * Class constants for the options array parameter used in many methods.
     */
    const OPT_LOAD      = 'load';       // load => true means load the object from the database.
    const OPT_DEEP      = 'deep';       // deep => true means load the object's children or parent as well.
    const OPT_SORT_BY   = 'sort_by';    // sort_by => 'colName' means sort the results by colName.
    const OPT_SORT_DIR  = 'sort_dir';   // sort_dir => 'ASC' means sort ascending, 'DESC' for descending.
    const OPT_WHERE     = 'where';      // where => 'xxx' allows for passing in additional where clause statements.
    const OPT_LIMIT     = 'limit';      // limit => 10 limits the query results to 10.
    const OPT_OFFSET    = 'offset';     // offset => 100 moves the query result set to the 100th result.
    const SORT_DIR_ASC  = 'ASC';        // ascending
    const SORT_DIR_DESC = 'DESC';       // descending
    const OPT_OPERATORS = 'operators';  // true enables tuple arguments in query (column name, operator, value)
                                        // @todo finish making this work, right now it will only inject $name $operator $value


    /**
     * Defines the database tablename for the AR object.
     * @static
     * @access public
     * @var    string
     */
    static public $tablename = null;

    /**
     * Defines the id column name for the database table.
     * @static
     * @access public
     * @var    string
     * @deprecated     now uses id_field in schema options
     */
    //static public $id_field  = 'id';


    /**
     * Flag to denote if the object can live in Memcache (or other cache).
     * @static
     * @access public
     * @var    bool
     */
    static public $cacheable = false;

    static public function getConnection($connName=null, $params=null) {
        return ARMREST_Database::getConnection($connName, $params);
    }

    /**
     * Returns a single AR objects matching the id passed in.
     *
     * @static
     * @access public
     * @param  int     $id       Id of the object in the database.
     * @param  array   $options  Key value pairs of find options. See class constants section above.
     * @param  bool    $log          True logs the query and parameters to file.
     * @return AR
     * @throws ARMREST_AR_Exception_UnpersistedRecord
     */
    static public function findById($id, $options=array(), $log=false) {
        //static::init();
        $result = null;
        if (static::getSchema()->getOption('cacheable')) {
            $cache    = ARMREST_AR_Cache::getInstance();
            $cacheKey = static::getCacheKey($id);
            $result   = $cache->get($cacheKey);
        }
        //if ($cache->getResultCode() === ARMREST_AR_Cache::RES_NOTFOUND)
        if (empty($result)) {
            if (!isset($options[self::OPT_LOAD])) $options[self::OPT_LOAD] = true;
            $res = static::find(array(static::getSchema()->getOption('id_field') => $id), $options, $log);
            if ($log) ARMREST_Logger::write(__CLASS__.'::'.__FUNCTION__.' > $res = '.print_r($res, true));
            if ($res instanceOf ARMREST_AR_Collection && count($res->getCollection()) == 1) {
                $result = $res[0];
                if (static::getSchema()->getOption('cacheable')) {
                    $cache->set($cacheKey, $result);
                }
            } else {
                throw new ARMREST_AR_Exception_UnpersistedRecord(get_called_class().' '.$id.' was not found!');
            }
        }
        return $result;
    }

    /**
     * Returns an ARMREST_AR_Collection of objects matching the properties passed in.
     *
     * @static
     * @access public
     * @param  array   $properties   Key value pairs to search against.
     * @param  array   $options      Key value pairs of find options. See class constants section above.
     * @param  bool    $log          True logs the query and parameters to file.
     * @return ARMREST_AR_Collection
     */
    static public function find($properties=array(), $options=array(), $log=false) {
        $where  = array();
        $schema = static::getSchema();
        $opersEnabled = isset($options[static::OPT_OPERATORS]) && $options[static::OPT_OPERATORS] ? true : false;
        //ARMREST_Logger::write('opersEnabled: '.print_r($opersEnabled, true));
        if (count($properties) > 0) {
            // handle any properties set to null
            $nulls = array();
            // also handle any "column IN (?, ?, ?...)" type of property values
            $in    = array();
            foreach ($properties as $name => $value) {
                if ($opersEnabled) {
                    $name = $value[0];
                    $oper = $value[1];
                    $val  = $value[2];
                } else {
                    $val  = $value;
                    $oper = '=';
                }
                try {
                    $colDef = $schema->getColumn($name);
                    if (is_null($val) && isset($colDef->value) && is_null($colDef->default)) {
                        $nulls[] = $name.' IS NULL';
                        unset($properties[$name]);
                    } else if (is_array($val)) {
                        $oper = !$opersEnabled ? 'IN' : $oper;
                        $in[] = $name.' '.$oper.' ('.join(',', array_values($value)).')';
                        unset($properties[$name]);
                    }
                } catch (ARMREST_AR_Exception_Undefined $e) {
                }
            }
            // convert properties to :name = ? for where statement
            $where = array_map(function($val) { return $val.' = :'.$val; }, array_keys($properties));
            // add null and in wheres to the where clauses
            $where = array_merge((array) $nulls, (array) $where, (array) $in);
        }
        // add in custom where clause
        if (isset($options[self::OPT_WHERE])) {
            $where[] = $options[self::OPT_WHERE];
        }
        $tablename = $schema->getOption('tablename');
        $sql = 'SELECT * FROM `'.$tablename.'`';
        if (!empty($where)) {
            $sql .= ' WHERE '.join(' AND ', $where);
        } 
        // Build ORDER BY clause
        $sortBy  = $schema->getOption('id_field');
        $sortDir = self::SORT_DIR_ASC;
        //if ($log) {
            //ARMREST_Logger::write('options: '.print_r($options, true));
        //}
        $columns = ARMREST_SchemaDefinition::get(get_called_class())->getColumns();
        $colNames = array();
        foreach ($columns as $c) $colNames[] = $c->name;
        //if (isset($options[self::OPT_SORT_BY]) && in_array($options[self::OPT_SORT_BY], array_keys(static::$db_fields))) {
        if (isset($options[self::OPT_SORT_BY]) && in_array($options[self::OPT_SORT_BY], $colNames)) {
            $sortBy = $options[self::OPT_SORT_BY];
        }
        if (isset($options[self::OPT_SORT_DIR])) {
            $sortDir = $options[self::OPT_SORT_DIR];
        }
        $sql .= ' ORDER BY '.$sortBy.' '.$sortDir;
        // Build LIMIT clause
        if (isset($options[self::OPT_LIMIT])) {
            $sql .= ' LIMIT '.$options[self::OPT_LIMIT];
            if (isset($options[self::OPT_OFFSET])) {
                $sql .= ' OFFSET '.$options[self::OPT_OFFSET];
            }
        }
        if ($opersEnabled) {
            $props = array();
            foreach ($properties as $name => $value) {
                $props[$name] = $value[2];
            }
            //ARMREST_Logger::write('props: '.print_r($props, true));
            $properties = $props;
        }
        if ($log) {
            ARMREST_Logger::write('sql: '.$sql);
            ARMREST_Logger::write('vars: '.print_r($properties, true));
        }
        $dbh = static::getConnection();
        //if ($log) ARMREST_Logger::write('dbh: '.print_r($dbh, true));
        $sth = $dbh->prepare($sql);
        $sth->execute($properties);
        $res = $sth->fetchAll(PDO::FETCH_ASSOC);
        $objs = array();
        $options[static::OPT_LOAD] = false;
        foreach ($res as $r) {
            //if ($log) ARMREST_Logger::write(get_called_class().'::find() instantiating r -> '.print_r($r, true));
            $o = new static($r, $options);
            $o->markClean();
            //if ($log) ARMREST_Logger::write(get_called_class().'::find() o = '.print_r($o, true));
            $objs[] = $o;
        }
        $collection = new ARMREST_AR_Collection($objs);
        return $collection;
    }


    static protected $definitionLoaded = array();

/*
    static public function init() {
        $class = get_called_class();
        if (!isset(static::$definitionLoaded[$class])) {
            $schema  = static::getSchema();
            $columns = array();
            foreach ($schema->getColumns() as $col) {
                $c            = new ARMREST_AR_Property($col->name);
                $c->type      = $col->type;
                $c->default   = $col->default;
                $c->required  = $col->required;
                $c->allowNull = $col->allowNull;
                $columns[$col->name] = $c;
            }
            //$class::$db_fields  = $columns;
            // set options for object
            $class::$tablename  = $schema->getOption('tablename');
            $class::$cacheable  = $schema->getOption('cacheable')  ? true : false;
            $class::$searchable = $schema->getOption('searchable') ? true : false;
            static::$definitionLoaded[$class] = true;
        }
    }
*


    /**
     * AR Object constructor.
     *
     * @access public
     * @param  mixed   $idOrProperties   Key value pair array or int.
     * @param  array   $options          See class constants above.
     * @return AR
     */
    public function __construct($idOrProperties=array(), $options=array()) {
        $this->preLoad();
        //static::init();
        if (!is_array($idOrProperties) && !is_null($idOrProperties)) {
            $idOrProperties = array(static::getSchema()->getOption('id_field') => $idOrProperties);
        }
        //error_log(get_called_class().' -> db_fields = '.print_r(static::$db_fields, true));
        $schema  = static::getSchema();
        //$columns = $schema->getColumns();
        //error_log(get_called_class().' -> columns = '.print_r($columns, true));
        foreach ($idOrProperties as $name => $value) {
            $colDef = $schema->getColumn($name);
            if (!$colDef) {
                throw new Exception('ARMREST_AR object '.get_called_class().' does not have property "'.$name.'" defined!');
            //} else {
            //    error_log($name.' column definition: '.print_r($colDef, true));
            }
            // @TODO fix this... I should probably just set the value to default when I instantiate...
            //       and then when I build the SQL check against defaults and remove from the SQL maybe?
            if ($colDef->type == PDO::PARAM_INT && $colDef->default == null && empty($value)) {
                // NOP
            } else {
                // @TODO fix this to use ->set() so we get all the benefit of validators etc.
                $this->dirtyProperties[$name] = $value;
            }
        }
        $id = $this->getId();
        if (!isset($options[self::OPT_LOAD])) {
            $options[self::OPT_LOAD] = true;
        }
        if ($options[self::OPT_LOAD] && !empty($id)) {
            $this->__load();
            $this->markClean();
        }
        $this->postLoad();
    }

    /**
     * Sets a property on the object that is stored in the database.
     *
     * @access public
     * @param  string   $name
     * @param  mixed    $value
     * @return void
     */
    public function set($name, $value) {
        if (!isset($this->properties[$name]) || $this->properties[$name] != $value) {
            if (ARMREST_SchemaDefinition::isValidValue(get_called_class(), $name, $value)) {
                $this->dirtyProperties[$name] = $value;
            } else {
                // throw an exception about invalid values.
                throw new ARMREST_AR_Exception_InvalidValue('Invalid value entered for '.get_called_class().'->'.$name);
            }
        }
    }
    public function __set($name, $value) {
        $colDef = ARMREST_SchemaDefinition::get(get_called_class())->getColumn($name);
        if ($colDef) {
            return $this->set($name, $value);
        } else {
            throw new ARMREST_AR_Exception_Undefined($name.' is not found in ARMREST_AR object schema!');
        }
    }

    /**
     * Gets a property on the object that is stored in the database.
     *
     * @access public
     * @param  string   $name
     * @return mixed
     */
    public function get($name) {
        $schema = static::getSchema();
        if (isset($this->dirtyProperties[$name])) {
            return $this->dirtyProperties[$name];
        } else if (isset($this->properties[$name])) {
            return $this->properties[$name];
        } else {
            $colDef = $schema->getColumn($name);
            if (isset($colDef->default) || $colDef->allowNull) {
                return $colDef->default;
            //} else if ($name != $schema->getOption('id_field') && $name != 'created_on') {
                //throw new Exception($name.' ARMREST_AR object schema has no default value defined!');
            }
        }
    }
    public function __get($name) {
        $colDef = ARMREST_SchemaDefinition::get(get_called_class())->getColumn($name);
        if ($colDef) {
            return $this->get($name);
        } else {
            throw new Exception($name.' is not foudn in ARMREST_AR object schema for '.get_called_class());
        }
    }

    public function getProperties() {
        return $this->properties;
    }

    /**
     * Returns the ID of the object.
     *
     * @todo add support for multi-column ids.
     * @access public
     * @return int
     */
    public function getId() {
        return $this->get(static::getSchema()->getOption('id_field'));
    }

    /**
     * Saves an object to the database.
     *
     * @access public
     * @return void
     */
    public function save() {
        $this->preSave();
        if ($this->isDirty()) {
            try {
                $id = $this->get(static::getSchema()->getOption('id_field'));
                if (!empty($id)) {
                    try {
                        $colDef = null;
                        if (static::getSchema()->hasColumn('modified_on')) {
                            $colDef = static::getSchema()->getColumn('modified_on');
                        } else {
                            $colDef = static::getSchema()->getColumn('updated');
                        }
                        if ($colDef) $this->set($colDef->name, date('Y-m-d H:i:s'));
                    } catch (ARMREST_AR_Exception_Undefined $e) {
                        // NOP -- table doesn't have modified_on column, can't store modified on date.
                    }
                    $this->update();
                } else {
                    try {
                        $colDef = null;
                        if (static::getSchema()->hasColumn('created_on')) {
                            $colDef = static::getSchema()->getColumn('created_on');
                        } else if (static::getSchema()->hasColumn('created')) {
                            $colDef = static::getSchema()->getColumn('created');
                        }
                        if ($colDef) $this->set($colDef->name, date('Y-m-d H:i:s'));
                    } catch (ARMREST_AR_Exception_Undefined $e) {
                        // NOP -- table doesn't have created_on column, can't store modified on date.
                    } 
                    $this->insert();
                }
                $this->markClean();
            } catch (ARMREST_AR_Exception_InsertFailed $e) {
                throw new ARMREST_AR_Exception_SaveFailed($e->getMessage(), $e->getCode(), $e);
            } catch (ARMREST_AR_Exception_UpdateFailed $e) {
                throw new ARMREST_AR_Exception_SaveFailed($e->getMessage(), $e->getCode(), $e);
            }
            $this->postSave();
        }
        return true;
    }


    public static function getSchema() {
        //error_log('getting schema for: '.get_called_class());
        return ARMREST_SchemaDefinition::get(get_called_class());
    }

    /**
     * Deletes an object from the database.
     *
     * @access public
     * @oaran  bool   $softDelete   If true, sets the deleted_on column instead of really deleting the object.
     * @return bool
     * @throws ARMREST_AR_Exception_UnpersistedRecord, ARMREST_AR_Exception_DeleteFailed
     */
    public function delete($softDelete=null) {
        $schema = static::getSchema();
        $id = $this->getId();
        if (!empty($id)) {
            $this->preDelete();
            // check for a deleted_on column before trying to do soft deletes
            $colDef = null;
            try {
                $colDef = $schema->getColumn('deleted_on');
            } catch (ARMREST_AR_Exception_Undefined $e) {
                // NOP
            }
            if ($colDef && $softDelete) {
                $this->set('deleted_on', date('Y-m-d H:i:s'));
                $retVal = $this->update();
            } else {
                $tablename = $schema->getOption('tablename');
                $id_field  = $schema->getOption('id_field');
                $sql = 'DELETE FROM `'.$tablename.'` WHERE `'.$id_field.'` = ?';
                $dbh = static::getConnection();
                $sth = $dbh->prepare($sql);
                $sth->execute(array($id));
                if ($sth->rowCount() == 0) {
                    throw new ARMREST_AR_Exception_DeleteFailed(get_class($this).' '.$this->getId().' does not exist in the database');
                }
                $this->clearCache();
                $retVal = true;
            }
            $this->postDelete();
            return $retVal;
        } else {
            throw new ARMREST_AR_Exception_UnpersistedRecord('Cannot delete an object that has not been saved first.');
        }
    }

    /**
     * Returns the permalink for the object.
     *
     * @access public
     * @return string
     */
    public function getPermaLink() {
        return '';
    }

    /**
     * Converts the AR object to a JSON representation in preparation for json_encode().
     *
     * @access public
     * @return array
     */
    public function toJson() {
        $ret = array(
            'properties' => $this->propertiesToJson(),
            'permalink' => $this->getPermaLink(),
        );
        return $ret;
    }

    /**
     * Converts the AR object's properties in preparation for json_encode().
     *
     * @access public
     * @return array
     */
    public function propertiesToJson() {
        $props   = array_merge($this->properties, $this->dirtyProperties);
        $columns = static::getSchema()->getColumns();
        foreach ($columns as $colDef) {
            $name = $colDef->name;
            $props[$name] = $this->get($name);
        }
        return $props;
    }

    public function jsonSerialize() {
        return $this->toJson();
    }





    protected $properties      = array();
    protected $dirtyProperties = array();



    /**
     * Returns the object's cache key generated with the id passed in.
     *
     * @static
     * @access protected
     * @param  int       $id
     * @return string
     */
    static protected function getCacheKey($id) {
        return join('-', array(get_called_class(), $id));
    }

    /**
     * Loads an object from the database.
     *
     * @access protected
     * @return void
     * @throws ARMREST_AR_Exception_UnpersistedRecord
     */
    protected function __load() {
        $this->preLoad();
        if (isset($id_field)) {
            $res = static::findById($this->getId(), array(static::OPT_LOAD => false, static::OPT_DEEP => false));
            if ($res) {
                foreach ($res->properties as $propName => $propValue) {
                    if (!empty($propValue)) {
                        $this->set($propName, $propValue);
                    }
                }
                $this->markClean();
            } else {
                throw new ARMREST_AR_Exception_UnpersistedRecord(get_class($this).' '.$this->getId().' does not exist in the database');
            }
        }
        $this->postLoad();
    }

    /**
     * Creates and executes the insert query to store the object in the database.
     *
     * @todo add exception for when insert fails.
     * @access protected
     * @return void
     */
    protected function insert() {
        $this->preInsert();
        $id_field  = static::getSchema()->getOption('id_field');
        $tablename = static::getSchema()->getOption('tablename');
        $sql = 'INSERT INTO `'.$tablename.'` (`'.join('`,`', array_keys($this->dirtyProperties)).'`) VALUES ('.join(',', array_map(function($val) { return ':'.$val; }, array_keys($this->dirtyProperties))).')';
        //ARMREST_Logger::write('sql: '.$sql);
        $dbh = static::getConnection();
        $sth = $dbh->prepare($sql);
        //ARMREST_Logger::write('properties: '.print_r($this->dirtyProperties, true));
        $sth->execute($this->dirtyProperties);
        $errInfo = $sth->errorInfo();
        if ($errInfo[0] !== '00000') {
            throw new ARMREST_AR_Exception_InsertFailed($errInfo[2], $errInfo[1]);
        }
        $id = $dbh->lastInsertId();
        if ($id) {
            //ARMREST_Logger::write('lastInsertId: '.$id);
            $this->set(static::getSchema()->getOption('id_field'), $id);
            $this->clearCache();
            $this->__load();
            $this->setCache();
            $this->postInsert();
            return true;
        } else {
            //ARMREST_Logger::write('no id for last insert!');
            throw new ARMREST_AR_Exception_InsertFailed('Could not get ID for last inserted object!');
        }
    }

    /**
     * Creates and executes the update query to store the object in the database.
     *
     * @todo add exception for when update fails.
     * @access protected
     * @return bool
     */
    protected function update() {
        $this->preUpdate();
        $id_field  = static::getSchema()->getOption('id_field');
        $tablename = static::getSchema()->getOption('tablename');
        $sql = 'UPDATE `'.$tablename.'` SET '.
               join(',', array_map(function($v) { return $v.' = :'.$v; }, array_keys($this->dirtyProperties))).
               ' WHERE '.$id_field.' = :id_where';
        //ARMREST_Logger::write('sql: '.$sql);
        $dbh = static::getConnection();
        $sth = $dbh->prepare($sql);
        //ARMREST_Logger::write('params: '.print_r($this->dirtyProperties, true));
        $sth->execute(array_merge(array('id_where' => $this->get($id_field)), $this->dirtyProperties));
        if ($sth->rowCount() == 0 && $sth->errorInfo()[0] != '0000') {
            throw new ARMREST_AR_Exception_UpdateFailed(get_called_class().' '.$this->getId().' could not be updated: '.print_r($sth->errorInfo(), true));
        }
        $this->clearCache();
        $this->__load();
        $this->setCache();
        $this->postUpdate();
        return true;
    }

    /**
     * Returns the object from cache.
     *
     * @access protected
     * @return mixed
     */
    protected function getCache() {
        $result = null;
        if (static::getSchema()->getOption('cacheable')) {
            $cache  = ARMREST_AR_Cache::getInstance();
            $result = $cache->get(static::getCacheKey($this->getId()));
        }
        return $result;
    }

    /**
     * Sets the object into the cache.
     *
     * @access protected
     * @return void
     */
    protected function setCache() {
        if (static::getSchema()->getOption('cacheable')) {
            $cache = ARMREST_AR_Cache::getInstance();
            $cache->set(static::getCacheKey($this->getId()), $this);
        }
    }

    /**
     * Removes the cache entry related to the object.
     *
     * @access protected
     * @return void
     */
    protected function clearCache() {
        if (static::getSchema()->getOption('cacheable')) {
            $cache = ARMREST_AR_Cache::getInstance();
            $cache->delete(static::getCacheKey($this->getId()));
        }
    }



    public function isDirty() {
        return sizeOf($this->dirtyProperties) > 0;
    }
    public function markClean() {
        $this->properties      = array_merge($this->properties, $this->dirtyProperties);
        $this->dirtyProperties = array();
    }


    public function getUUIDForColumn($colName) {
        $numAttempts = 0;
        $uuid = null;
        do {
            $id = $this->get_uuid();
            $rs = static::find(array($colName => $id));
            if ($rs->count() == 0) {
                $uuid = $id;
            }
            $numAttempts++;
        } while (is_null($uuid) && $numAttempts < 10);
        if (is_null($uuid)) {
            throw new Exception('Unable to generate UUID for '.$colName.' on '.get_called_class());
        }
        return $uuid;
    }

    protected function gen_uuid() {
        return sprintf( '%04x%04x-%04x-%04x-%04x-%04x%04x%04x',
            // 32 bits for "time_low"
            mt_rand( 0, 0xffff ), mt_rand( 0, 0xffff ),
    
            // 16 bits for "time_mid"
            mt_rand( 0, 0xffff ),

            // 16 bits for "time_hi_and_version",
            // four most significant bits holds version number 4
            mt_rand( 0, 0x0fff ) | 0x4000,

            // 16 bits, 8 bits for "clk_seq_hi_res",
            // 8 bits for "clk_seq_low",
            // two most significant bits holds zero and one for variant DCE1.1
            mt_rand( 0, 0x3fff ) | 0x8000,
    
            // 48 bits for "node"
            mt_rand( 0, 0xffff ), mt_rand( 0, 0xffff ), mt_rand( 0, 0xffff )
        );
    }

    protected function preLoad() {
        return true;
    }
    protected function postLoad() {
        return true;
    }

    protected function preSave() {
        return true;
    }
    protected function postSave() {
        return true;
    }

    protected function preInsert() {
        return true;
    }
    protected function postInsert() {
        return true;
    }

    protected function preUpdate() {
        return true;
    }
    protected function postUpdate() {
        return true;
    }

    protected function preDelete() {
        return true;
    }
    protected function postDelete() {
        return true;
    }

}

