<?php
/**
 * @author Philip Snyder <philip.r.snyder@gmail.com>
 */

define('PDO_PARAM_BOOL', PDO::PARAM_BOOL);
define('PDO_PARAM_NULL', PDO::PARAM_NULL);
define('PDO_PARAM_INT',  PDO::PARAM_INT);
define('PDO_PARAM_STR',  PDO::PARAM_STR);
define('PDO_PARAM_LOB',  PDO::PARAM_LOB);


/**
 * Manages reading configuration files and fetching values.
 * @todo take Zend config class and implement it without requiring all of zend. I like it better than mine is why.
 */
class ARMREST_Config {

    /**
     * Default configuration file name.
     */
    const DEFAULT_CONFIG = 'default';


    static public function getEnvironment() {
        // copied from CodeIgniter index.php
        if (!defined('ENVIRONMENT')) {
           if (getenv('ENVIRONMENT')) {
               define('ENVIRONMENT', getenv('ENVIRONMENT'));
           }
           defined('ENVIRONMENT') || define('ENVIRONMENT', (getenv('APPLICATION_ENV') ? getenv('APPLICATION_ENV') : 'development'));
        }
        return ENVIRONMENT;
    }


    /**
     * Returns a config instance, loaded statically.
     *
     * @static
     * @access public
     * @param  string  $configFile  Configuration file to load (without extension). Defaults to static::DEFAULT_CONFIG (ARMREST_Config::DEFAULT_CONFIG => 'default').
     * @param  bool    $force       Forces a reload of the configuration file. Defaults to false.
     * @return ARMREST_Config
     */
    static public function getInstance($configFile=null, $force=false) {
        if (is_null($configFile)) {
            $configFile = static::DEFAULT_CONFIG;
        }
        if (!isset(static::$instances[$configFile])) {
            static::$instances[$configFile] = new static($configFile, $force);
        }
        return static::$instances[$configFile];
    }





    /**
     * Static array to hold instances of Config class.
     *
     * @static
     * @access protected
     * @var    array  $instances
     */
    static protected $instances = array();

    /**
     * Generates the config file's path.
     * Note: DO NOT store the value in the object for security purposes as that would give away filesystem path information.
     *
     * @TODO fix this to NOT use a relative path to the file --- OR change the relative path to work with composer vendor
     * @TODO add support for environment + default settings (ie 2 config files)
     *
     * @static
     * @final
     * @access protected
     * @param  string  $configFile
     * @return string
     */
    static final protected function getConfigFilePath($configFile) {
        $envCfgPath = join(
            DIRECTORY_SEPARATOR,
            array(
                dirname(__FILE__),
                '..', '..', '..', '..', '..', // I need to find a better way to get the root path
                'config',
                static::getEnvironment(),
                $configFile.'.ini'
            )
        );
        if (file_exists($envCfgPath)) {
            return $envCfgPath;
        } else {
            // @todo add file_exists / readable checks & throw fancy exceptions instead of fatal errors
            return join(
                DIRECTORY_SEPARATOR,
                array(
                    dirname(__FILE__),
                    '..', '..', '..', '..', '..', // I need to find a better way to get the root path
                    'config',
                    $configFile.'.ini'
                )
            );
        }
    }




    /**
     * Returns the value from a config variable.
     * 
     * @param  string  $section   Config section as identified by [section_name] lines according to ini format.
     * @param  string  $name      Config variable name.
     * @return mixed
     * @throws OutOfRangeException
     */
    public function get($section, $name=null) {
        $this->load();
        if (!is_null($name)) {
            if (!isset($this->configData[$section][$name])) {
                throw new OutOfRangeException('Config variable not set: '.$section.'->'.$name);
            }
            return $this->configData[$section][$name];
        } else {
            return $this->configData[$section];
        }
    }




    /**
     * Holds the config file name (without extension).
     *
     * @access protected
     * @var   $configFile
     * @type  string
     */
    protected $configFile = null;

    /**
     * Holds the config file data after being parsed.
     *
     * @access protected
     * @var    $configData
     * @type   array<mixed>
     */
    protected $configData = null;

    /**
     * Constructor for Config class that has access restricted to require instantiation via static::getInstance().
     * 
     * @access protected
     * @param  string  $configFile  Configuration file to load (without extension). Defaults to static::DEFAULT_CONFIG (Config::DEFAULT_CONFIG => 'default').
     * @param  bool    $force       Forces a reload of the configuration file. Defaults to false.
     * @return Config
     * @throws Exception_FileNotFound
     */
    protected function __construct($configFile=null, $force=false) {
        if (is_null($configFile)) {
            $configFile = static::DEFAULT_CONFIG;
        }
        $filePath = static::getConfigFilePath($configFile);
        if (!file_exists($filePath)) {
            throw new ARMREST_Exception_FileNotFound('Configuration file not found: '.$configFile);//.' -> '.$filePath);
        }
        $this->configFile = $configFile;
    }

    /**
     * Parses the config ini file if it has not been parsed already or is forced to do so.
     *
     * @access protected
     * @param  bool  $force  Forces parsing of the file, Default is false.
     */
    protected function load($force=false) {
        if ($force || is_null($this->configData)) {
            $this->configData = parse_ini_file(static::getConfigFilePath($this->configFile), true);
        }
    }

}

