<?php
/**
 * @copyright Copyright 2014 Philip Snyder <philip.r.snyder@gmail.com>
 * // @license http://github.com/basdenooijer/solarium/raw/master/COPYING
 * // @link http://www.solarium-project.org/
 *
 * @package ARMREST
 */

/**
 * Autoloader
 *
 * This class is included to allow for easy usage of ARMREST.
 *
 * @package ARMREST
 */
class ARMREST_Autoloader {

    /**
     * Register the ARMREST autoloader
     *
     * The autoloader only acts for classnames that start with 'ARMREST'. It
     * will be appended to any other autoloaders already registered.
     *
     * Using this autoloader will disable any existing __autoload function. If
     * you want to use multiple autoloaders please use spl_autoload_register.
     *
     * @static
     * @access public
     * @return void
     */
    static public function register() {
        error_log('registering autoloader for ARMREST');
        spl_autoload_register(array(new self, 'load'));
    }

    /**
     * Autoload a class
     *
     * This method is automatically called after registering this autoloader.
     * The autoloader only acts for classnames that start with 'ARMREST'.
     *
     * @static
     * @access public
     * @param string $class
     * @return void
     */
    static public function load($class) {
        if (strpos($class, 0, 7) == 'ARMREST') {
            $class = str_replace(
                array('ARMREST', '_'),
                array('', '/'),
                $class
            );
            $file = dirname(__FILE__).'/'.$class.'.php';
            error_log('require_once('.$file.')');
            require_once($file);
        }
    }

}
