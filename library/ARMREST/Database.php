<?php
/**
 * @author Philip Snyder <philip.r.snyder@gmail.com>
 */

/**
 * Handles connection to the database, using config/db.ini for configuration options.
 *
 */
class ARMREST_Database {

    /**
     * Defines default connection key.
     */
    const CONNECTION_DEFAULT = 'default';

    /**
     * Holds instances of database connections for later use.
     *
     * @static
     * @access protected
     */
    protected static $connections = array();

    /**
     * Instantiates and returns PDO connection reqeusted.
     *
     * @static
     * @access public
     * @param  string  $connType   Label of the database connection (from db.ini config file)
     * @return PDO
     */
    public static function getConnection($connType=self::CONNECTION_DEFAULT, $params=null) {
        if (is_null($params)) $params = array();
        $config = ARMREST_Config::getInstance('db');
        if (!isset(static::$connections[$connType])) {
            //error_log('reading connection info for '.$connType);
            $cfg    = $config->get($connType);
            //error_log('cfg: '.print_r($cfg, true));
            if (!$cfg) {
                throw new Exception('Database connection "'.$connType.'" is not defined!');
            }
            $cfg    = array_merge($cfg, $params);
            $dsn    = $cfg['driver'].":host=".$cfg['hostname'].";port=".$cfg['port'].";dbname=".$cfg['database'];
            $conn   = new PDO($dsn, $cfg['username'], $cfg['password']);
            //$conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
            if ($conn) {
                static::$connections[$connType] = $conn;
            } else {
                throw new Exception('Unable to open database connection: '.$connType);
            }
        //} else {
        //    error_log('using existing db connection for '.$connType);
        }
        return static::$connections[$connType];
    }

    public static function getConnections() {
        return static::$connections;
    }

}

