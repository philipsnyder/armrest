<?php
/**
 * @author Philip Snyder <philip.r.snyder@gmail.com>
 */


/**
 * Handles reading ARMREST_AR object schema files.
 *
 */
class ARMREST_SchemaDefinition {

    /**
     * Static array to hold schema definitions.
     *
     * @static
     * @access protected
     * @var    array
     */
    static protected $instances = array();

    /**
     * This gets populates with the decoded json data we read from the file.
     *
     * @access protected
     * @var    mixed
     */
    protected $data = null;

    /**
     * Holds the name of the ARMREST_AR object that this schema defines.
     *
     * @access protected
     * @var    string
     */
    protected $className = null;




    /**
     * Returns a schema definition, stored statically.
     *
     * @static
     * @access public
     * @param  string  $className   Schema file to load (without extension).
     * @return ARMREST_SchemaDefinition
     */
    static public function get($className, $read=true) {
        if (!isset(static::$instances[$className])) {
            static::$instances[$className] = new static($className, $read);
        }
        return static::$instances[$className];
    }


    /**
     * Checks to see if a value passes the defined validation rules and property type checks.
     *
     * @TODO add in support for validator rules and type checks here!!!!
     *
     * @static
     * @access public
     * @param  string   $className
     * @param  string   $propertyName
     * @param  mixed    $value
     * @return bool
     */
    static public function isValidValue($className, $propertyName, $value) {
        return true;
    }

    /**
     * Generates the schema definition file's path.
     * Note: DO NOT store the value in the object for security purposes as that would give away filesystem path information.
     *
     * @TODO fix this to NOT use a relative path to the file --- OR change the relative path to work with composer vendor
     *
     * @static
     * @final
     * @access protected
     * @param  string  $className
     * @return string
     */
    static final protected function getFilePath($className) {
        return join(
            DIRECTORY_SEPARATOR,
            array(
                dirname(__FILE__),
                '..', '..', '..', '..', '..',
                'config', 'schema',
                str_replace('_', DIRECTORY_SEPARATOR, $className).'.json'
            )
        );
    }




    /**
     * Constructor for class that has access restricted to require instantiation via static::getInstance().
     * 
     * @access protected
     * @param  string  $className   Schema definition to load (without extension).
     * @return SchemaDefinition
     * @throws ARMREST_Exception_FileNotFound
     */
    protected function __construct($className, $read=true) {
        if (!file_exists(static::getFilePath($className)) && $read) {
            throw new ARMREST_Exception_FileNotFound('ARMREST object schema not found for "'.$className.'"!');
        }
        $this->className = $className;
        if ($read) {
            $json = trim(file_get_contents(static::getFilePath($className)));
            $this->data = json_decode($json);
            if (empty($this->data)) {
                error_log('json_last_error() '.json_last_error());
            }
        }
    }


    /**
     * Returns a specific column definition defined in an object's schema.
     * 
     * @access public
     * @param  string  $name      Column name.
     * @return mixed
     * @throws OutOfRangeException
     */
    public function getColumn($name) {
        foreach ($this->data->columns as $col) {
            if ($col->name == $name) {
                return $col;
            }
        }
        //return null;
        throw new ARMREST_AR_Exception_Undefined('Column "'.$name.'" not found in ARMREST_AR object schema definition for "'.$this->className.'"');
    }

    public function hasColumn($name) {
        foreach ($this->data->columns as $col) {
            if ($col->name == $name) return true;
        }
        return false;
    }

    /**
     * Returns the columns defined in an object's schema.
     *
     * @access public
     * @return stdClass
     */
    public function getColumns() {
        return $this->data->columns;
    }

    public function addColumn($col) {
        $this->data->columns[] = $col;
    }

    /**
     * Returns the value for a specific option defined in an object's schema.
     *
     * @param  string  $name      Column name.
     * @access public
     * @return mixed
     * @throws OutOfRangeException
     */
    public function getOption($name) {
        return $this->data->options->$name;
    }

    /**
     * Returns the options defined in an object's schema.
     *
     * @access public
     * @return stdClass
     */
    public function getOptions() {
        return $this->data->options;
    }

    public function addOption($name, $value) {
        if (!isset($this->data->options)) {
            $this->data->options = new stdClass;
        }
        $this->data->options->$name = $value;
    }


    public function toJson() {
        return $this->data;
    }

}

