<?php
/**
 * @author Philip Snyder <philip.r.snyder@gmail.com>
 */

/**
 * Defines a basic error & exception handler that writes out via ARMREST_Logger.
 *
 *
 *
 */
class ARMREST_ErrorHandler {

    /**
     * 1	E_ERROR
     * 2	E_WARNING
     * 4	E_PARSE
     * 8	E_NOTICE
     * 16	E_CORE_ERROR
     * 32	E_CORE_WARNING
     * 64	E_COMPILE_ERROR
     * 128	E_COMPILE_WARNING
     * 256	E_USER_ERROR
     * 512	E_USER_WARNING
     * 1024	E_USER_NOTICE
     * 6143	E_ALL
     * 2048	E_STRICT
     * 4096	E_RECOVERABLE_ERROR
     */
    
    static public function error_handler($errno, $errstr, $errfile, $errline, $errcontext) {
        $errRptLvl = error_reporting();
        // Validate this is an error level we're configured to report
        if (!($errno & $errRptLvl)) {
            return false;
        }
        $e = new Exception($errstr);
        static::exception_handler($e);
    }

    static public function exception_handler($exception) {
        ARMREST_Logger::write("Uncaught Exception: ".$e);
    }

}




set_error_handler(array('ARMREST_ErrorHandler', 'error_handler'));
set_exception_handler(array('ARMREST_ErrorHandler', 'exception_handler'));


