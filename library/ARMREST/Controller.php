<?php



class ARMREST_Controller {

    const METHOD_GET     = 'GET';
    const METHOD_POST    = 'POST';
    const METHOD_PUT     = 'PUT';
    const METHOD_DELETE  = 'DELETE';
    const METHOD_PATCH   = 'PATCH';
    const METHOD_OPTIONS = 'OPTIONS';

    static protected $supported_formats = array(
        'xml'        => 'application/xml',
        'json'       => 'application/json',
        'jsonp'      => 'application/javascript',
        'serialized' => 'application/vnd.php.serialized',
        'php'        => 'text/plain',
        'html'       => 'text/html',
        'csv'        => 'application/csv'
    );

    static protected $instance = null;

    static public function getInstance() {
        $route = static::getRoute();
        if (is_null(static::$instance)) {
            //Logger::write(__CLASS__.'::'.__FUNCTION__.'() -> route: '.print_r($route, true));
            //Logger::write('_REQUEST: '.print_r($_REQUEST, true));
            $controller = $route->controller;
            static::$instance = new $controller;
        }
        return static::$instance;
    }

    static protected function getRoute() {
        static $route = null;
        if (!is_null($route)) {
            return $route;
        }
        $supportedFormats = array_keys(static::$supported_formats);
        $routes  = static::getRoutes();
        $method  = strtolower($_SERVER['REQUEST_METHOD']);
        // hold on to the original URI
        $uri     = $_SERVER['REQUEST_URI'];
        if (strtoupper($method) == 'GET') {
            //Logger::write('input: '.print_r($_REQUEST, true));
            $uri = str_replace('?'.$_SERVER['QUERY_STRING'], '', $uri);
        }
        $origUri = $uri;
        $format  = 'json';
        // see if we have a format at the end of the request URI
        if (preg_match('/\.('.join('|', $supportedFormats).')$/', strtolower($uri), $matches)) {
            $format = $matches[1];
            $uri    = preg_replace('/\.(json|xml|html)$/', '', $uri);
        }
        /*Logger::write("uri:    $uri\n".
                      "method: $method\n".
                      "format: $format\n");*/
        $uriPieces = explode('/', trim($uri, '/'));
        //Logger::write('uri pieces: '.print_r($uriPieces, true));
        foreach ($routes as $r => $v) {
            $params     = array();
            // we say it matches until we prove otherwise
            $routeMatch = true;
            $pieces     = explode('/', $r);
            //Logger::write("route: $r\n".'pieces: '.print_r($pieces, true));
            foreach ($pieces as $idx => $p) {
                if (!isset($uriPieces[$idx])) {
                    $routeMatch = false;
                    break;
                }
                //Logger::write("pieces[$idx]: $p\n".
                //              "uriPieces[$idx]: ".$uriPieces[$idx]."\n");
                if ($p != $uriPieces[$idx]) {
                    if (preg_match('/^\:([^\/]+)/', $p, $matches)) {
                        $params[$matches[1]] = array($matches[1], '=', $uriPieces[$idx]);
                    } else {
                        $routeMatch = false;
                        break;
                    }
                }
            }
            if (!$routeMatch) continue;
            // save our param pieces for later
            $paramPieces = array_slice($uriPieces, sizeOf($pieces));
            // now replace the request URI w/ the actual route
            $rV = $v;
            foreach ($params as $key => $val) {
                $rV = str_replace(':'.$key, $val[2], $rV);
            }
            // grab additional input from _GET & _POST as necessary
            $input = array();
            switch (strtoupper($method)) {
                case 'GET':    $input = $_GET;  break;
                case 'POST':   $input = $_POST; break;
                case 'PUT':
                    $fh  = fopen('php://input', 'r');
                    /*$str = '';
                    while ($s = fread($fh, 1024)) {
                        $str .= $s;
                    }*/
                    $str = file_get_contents("php://input");
                    //Logger::write(__CLASS__.'::'.__FUNCTION__.' -> str = '.print_r($str, true));
                    parse_str($str, $input);
                    break;
                case 'DELETE':
            }
            //Logger::write(__CLASS__.'::'.__FUNCTION__.' -> input = '.print_r($input, true));
            // convert input array to our format w/ operators
            foreach ($input as $key => $val) {
                if (is_array($val) && sizeOf($val) == 3) {
                    $params[$key] = $val;
                } else {
                    $params[$key] = array($key, '=', $val);
                }
            }
            // parse any additional params
            //Logger::write('param pieces: '.print_r($paramPieces, true)."\n");
            if ((sizeOf($paramPieces) % 3) !== 0) {
                throw new Exception("param pieces does not contain valid tuples: ".print_r($paramPieces, true));
            } else {
                while (sizeOf($paramPieces) > 0) {
                    $key  = array_shift($paramPieces);
                    $oper = array_shift($paramPieces);
                    $val  = array_shift($paramPieces);
                    $params[$key] = array($key, $oper, urldecode($val));
                }
            }
            //Logger::write('params: '.print_r($params, true)."\n");
    
            $rvp = explode('/', $rV);
            $route             = new stdClass;
            $route->origUri    = $origUri;
            $route->uri        = $rV;
            $route->controller = $rvp[0];
            $route->function   = $rvp[1];
            $route->method     = $method;
            $route->params     = $params;
            $route->format     = strtolower($format);
    
            Logger::write("ROUTE FOUND for $origUri\n".
                          "route: $rV\n".
                          "method: ".$route->method."\n".
                          "format: ".$route->format."\n".
                          "params: ".print_r($params, true)."\n");
            return $route;
        }
    }

    protected function getParams() {
/*
        if (empty($this->params)) {
            $this->params = array();
            switch (strtoupper($_SERVER['REQUEST_METHOD'])) {
                case static::METHOD_POST:
                    //$obj = json_decode(file_get_contents("php://input"), true);
                    //$this->params = $obj;
                    $this->params = $_POST;
                    break;
                case static::METHOD_GET:
                    //$obj = json_decode(file_get_contents("php://input"), true);
                    $this->params = $_GET;//$obj;
                    break;
                case static::METHOD_PUT:
                    $obj = json_decode(file_get_contents("php://input"), true);
                    $this->params = $obj;
                    break;
                case static::METHOD_DELETE:
                    //$obj = json_decode(file_get_contents("php://input"), true);
                    //error_log('DELETE '.print_r($obj, true));
$route = static::getRoute();
error_log('route: '.print_r($route, true));
$uri   = $_SERVER["REQUEST_URI"];
error_log('uri: '.$uri);
$p = array_values(array_filter(explode('/', $str)));
                    $this->params = $p;
                    break;
                default:
                    error_log('method is '.$_SERVER['REQUEST_METHOD'].', request = '.print_r($_REQUEST, true));
                    break;
            }
            $pattern = '/'.$this->route->controller.'\/'.$this->route->function.'\/([0-9]+)\.'.$this->route->format.'/';
            if (preg_match($pattern, $this->route->uri, $matches)) {
                $this->params = array_merge(array('id' => $matches[1]), $this->params);
            }
        }*/
        return $this->route->params;
    }

    protected $controller = null;
    protected $function   = null;
    protected $method     = null;
    protected $format     = null;
    protected $params     = array();

    protected function __construct() {
//Logger::write(__CLASS__.'::'.__FUNCTION__.' called, session = '.print_r($_SESSION, true));
        $this->setOrigin();
        $route            = static::getRoute();
        $this->route      = $route;
        $this->controller = $route->controller;
        $this->function   = $route->function;
        $this->method     = strtolower($route->method);
        $this->format     = $route->format;
    }


    protected function handle() {
        $function = $this->function.'_'.$this->method;
        if (!method_exists(get_called_class(), $function)) {
            $msg = get_called_class().'->'.$function.'() is not defined.';
            //error_log(__FILE__.': '.$msg);
            throw new ARMREST_Controller_Exception_Undefined($msg);
        }
        return $this->$function();
    }

    public function respond() {
        if (strtoupper($_SERVER['REQUEST_METHOD']) == static::METHOD_OPTIONS) {
            //error_log('OPTIONS preflight call received');
            foreach ($this->getHeaders() as $h) {
                header($h);
            }
            //header('Status: 200');
        } else {
            try {
                $resp = '';
                $obj  = $this->handle();
                foreach ($this->getHeaders() as $header) {
                    header($header);
                }
                //error_log('obj: '.print_r($obj, true));
                //error_log('format: '.print_r($this->format, true));
                switch ($this->format) {
                    case 'json':
                        if ($obj instanceOf ARMREST_AR_Interface_toJson) {
                            //error_log('converting to json...');
                            $obj = $obj->toJson();
                        }
                        $resp = json_encode($obj);
                        break;
                    case 'html':
                        $resp = $obj;
                        break;
                    case 'jsonp':
                    case 'xml':
                    default:
                        break;
                }
                echo $resp;
            } catch (ARMREST_AR_Exception_UnpersistedRecord $e) {
                http_response_code(404);
            } catch (ARMREST_Controller_Exception_Undefined $e) {
                http_response_code(404);
            }
        }
    }

    protected function setOrigin() {
    }

    protected function getOrigin() {
        $requestHeaders = getallheaders();
        // @todo add checks for access via API key + origin
        if (isset($requestHeaders['Origin'])) {
            return $requestHeaders['Origin'];
        } else {
            //error_log('no origin found, using wildcard...');
            return '*';
        }
    }

    protected function getExposedHeaders() {
        return array();
    }

    protected function getContentType() {
        return 'text/'.$this->format;
    }

    protected function getHeaders() {
        $headers = array(
            'Access-Control-Allow-Methods: GET,POST,PUT,DELETE,OPTIONS',
            'Access-Control-Allow-Credentials: true',
            'Access-Control-Allow-Origin: '.$this->getOrigin(),
            'Access-Control-Allow-Headers: Content-Type',
            'Access-Control-Expose-Headers: '.join(',', $this->getExposedHeaders()),
            'Content-Type: '.$this->getContentType(),
        );
        return $headers;
    }



    static protected function getRoutes() {
        $routes = ARMREST_Config::getInstance('controllers')->get('routes');
        return $routes;
    }


}






