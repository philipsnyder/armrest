<?php
/**
 * @author Philip Snyder <philip.r.snyder@gmail.com>
 */

/**
 * Basic Logging class. Currently only writes to file.
 * @todo extend to allow writing to db, email, whatever.
 *
 */
class ARMREST_Logger {

    /**
     * Holds loggers that have been instantiated.
     *
     * @static
     * @access protected
     * @var    array<ARMREST_Logger>
     */
    protected static $instances = array();

    /**
     * File resource handle.
     *
     * @access protected
     * @var    resource
     */
    protected $handle = null;

    /**
     * Log file name (just the file name, not the extension or path).
     *
     * @acces protected
     * @var   string
     */
    protected $logFile = null;


    /**
     * Constructor is restricted to enforce getInstance() call.
     *
     * @access protected
     * @param  string     $filename  Log file name, for instance: logs/default.log => default
     * @return ARMREST_Logger
     */
    protected function __construct($filename) {
        $filename = realpath(
            join(
                DIRECTORY_SEPARATOR,
                array(dirname(__FILE__), '..', '..', '..', '..', '..', 'logs')
            )
        ).DIRECTORY_SEPARATOR.$filename.'.log';
        //error_log('Logger filename: '.$filename);
        $this->logFile = $filename;
        //$this->open();
    }

    /**
     * Cleanup.
     *
     * @access public
     * @return void
     */
    public function __destruct() {
        $this->close();
    }

    /**
     * Closes the file resouce handle.
     *
     * @access private
     * @return void
     */
    private function close() {
        if (!is_null($this->handle)) fclose($this->handle);
    }

    /**
     * Opens a resource handle to the file.
     *
     * @access private
     * @return void
     */
    private function open() {
        if (is_null($this->handle)) $this->handle = fopen($this->logFile, 'a');
    }

    /**
     * Returns an instance of the logger with the file requested.
     *
     * @static
     * @access public
     * @param  string  $filename
     * @return ARMREST_Logger
     */
    public static function getInstance($filename=null) {
        if (is_null($filename)) $filename = 'default';
        if (!isset(static::$instances[$filename])) static::$instances[$filename] = new static($filename);
        return static::$instances[$filename];
    }


    /**
     * Writes a line to the logger's file.
     *
     * @static
     * @access public
     * @param  string  $msg
     * @param  string  $filename   Defaults to 'default'
     * @return void
     */
    public static function write($msg, $filename=null) {
        $instance = static::getInstance($filename);
        $instance->open();
        fputs($instance->handle, $msg."\n");
    }

}


 



