<?php



class ARMREST_AR_Property {

    public $name          = null;
    public $type          = null;
    public $default       = null;
    public $required      = null;
    public $allowNull     = null;
    public $validateRules = array();

    public function __construct($name) {
        $this->name = $name;
    }

    public function isValid($value) {
        $validator = new ARMREST_AR_Validator($this->validateRules);
        return $validator->isValid($value);
    }

}


