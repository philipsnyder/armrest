<?php


abstract class ARMREST_AR_Tree extends ARMREST_AR {

    static public $cacheable  = false;
    static public $searchable = false;


    public $parent   = null;
    public $children = null;

    public function __construct($idOrProperties=array(), $options=array()) {
        parent::__construct($idOrProperties, $options);
        $id = $this->getId();
        if (!empty($id)) {
            $this->loadParent();
            if (isset($options[self::OPT_DEEP]) && $options[self::OPT_DEEP]) {
                $this->loadChildren();
            }
        }
    }


    public function loadChildren($force=false) {
        if ($force || is_null($this->children)) {
            $res = static::find(array('parent_id' => $this->getId()), array(self::OPT_LOAD => false, self::OPT_DEEP => false, self::OPT_SORT_BY => 'name'));
            $this->children = $res->getCollection();
        }
    }

    public function loadParent($force=false) {
        if (isset($this->properties['parent_id']) && !is_null($this->properties['parent_id']) && ($force || is_null($this->parent))) {
            $this->parent = static::findById($this->get('parent_id'), array(self::OPT_LOAD => false, self::OPT_DEEP => false));
        }
    }


    public function getBreadcrumb() {
        $class = strtolower(str_replace('AP_', '', get_class($this)));
        return ($this->parent ? $this->parent->getBreadcrumb().' &raquo; ' : '').'<span data-class="'.$class.'" data-id="'.$this->getId().'" class="'.$class.'">'.$this->get('name').'</span>';
    }

    public function toJson($deep=true) {
        if ($deep) {
            $this->loadChildren();
            $this->loadParent();
        }
        $ret = parent::toJson();
        $ret['breadcrumb'] = $this->getBreadcrumb();
        $ret['parent'] = $this->parent ? $this->parent->toJson(false) : null;
        if (is_array($this->children)) {
            foreach ($this->children as $c) {
                $ret['children'][] = $c->toJson(false);
            }
        }
        return $ret;
    }

    protected function setCache() {
        parent::setCache();
        $ids   = static::getAncestorIds($this->getId());
        $cache = ARMREST_AR_Cache::getInstance();
        foreach ($ids as $id) {
            $cache->delete(static::getCacheKey($id));
            $cache->delete(get_called_class().'-getAncestorIds-'.$id);
            $cache->delete(get_called_class().'-getAncestors-'.$id);
            $cache->delete(get_called_class().'-getDescendantIds-'.$id);
            $cache->delete(get_called_class().'-getDescendants-'.$id);
        }
    }

    public static function getAncestors($nodeId, $log=false) {
        $null     = null;
        $useCache = false;
        $cacheKey = get_called_class().'-'.__FUNCTION__.'-'.$nodeId;
        if (static::$cacheable) {
            $cache    = ARMREST_AR_Cache::getInstance();
            $results  = $cache->get($cacheKey, $null, $null, $log);
            if ($cache->getResultCode() !== ARMREST_AR_Cache::RES_NOTFOUND) {
                $useCache = true;
            }
        }
        if (!$useCache) {
            if ($log) ARMREST_Logger::write(get_called_class().'::'.__FUNCTION__.': fetching results; key: '.$cacheKey);
            $nodeIds = array($nodeId);
            $results = array();
            while (sizeOf($nodeIds) > 0) {
                $sNodeId   = array_shift($nodeIds);
                $node      = static::findById($sNodeId, array(static::OPT_LOAD => false, static::OPT_DEEP => false));
                $results[] = $node;
                $parentId  = $node->get('parent_id');
                if (!empty($parentId)) {
                    $nodeIds[] = $parentId;
                }
            }
            if (static::$cacheable) {
                $cache->set($cacheKey, $results, 0, $log);
            }
        } else if ($log) {
            ARMREST_Logger::write(get_called_class().'::'.__FUNCTION__.': using cached results; key: '.$cacheKey);
        }
        return new ARMREST_AR_Collection($results);
    }

    public static function getAncestorIds($nodeId, $log=false) {
        $null     = null;
        $useCache = false;
        $cacheKey = get_called_class().'-'.__FUNCTION__.'-'.$nodeId;
        if (static::$cacheable) {
            $cache    = ARMREST_AR_Cache::getInstance();
            $results  = $cache->get($cacheKey, $null, $null, $log);
            if ($cache->getResultCode() !== ARMREST_AR_Cache::RES_NOTFOUND) {
                $useCache = true;
            }
        }
        if (!$useCache) {
            if ($log) ARMREST_Logger::write(get_called_class().'::'.__FUNCTION__.': fetching results; key: '.$cacheKey);
            $results = array();
            $res     = static::getAncestors($nodeId);
            foreach ($res->getCollection() as $r) {
                $results[] = $r->getId();
            }
            if (static::$cacheable) {
                $cache->set($cacheKey, $results, 0, $log);
            }
        } else if ($log) {
            ARMREST_Logger::write(get_called_class().'::'.__FUNCTION__.': using cached results; key: '.$cacheKey);
        }
        return $results;
    }

    public static function getDescendants($nodeId, $log=false) {
        $null     = null;
        $useCache = false;
        $cacheKey = get_called_class().'-'.__FUNCTION__.'-'.$nodeId;
        if (static::$cacheable) {
            $cache    = ARMREST_AR_Cache::getInstance();
            $results  = $cache->get($cacheKey, $null, $null, $log);
            if ($cache->getResultCode() !== ARMREST_AR_Cache::RES_NOTFOUND) {
                $useCache = true;
            }
        }
        if (!$useCache) {
            if ($log) ARMREST_Logger::write(get_called_class().'::'.__FUNCTION__.': fetching results; key: '.$cacheKey);
            $nodeIds = array($nodeId);
            $results = array(new static($nodeId));
            while (sizeOf($nodeIds) > 0) {
                $sNodeId = array_shift($nodeIds);
                $res = static::find(array('parent_id' => $sNodeId), array(static::OPT_LOAD => false, static::OPT_DEEP => false, static::OPT_SORT_BY => 'name'), false);
                foreach ($res->getCollection() as $r) {
                    $nodeIds[] = $r->getId();
                    $results[] = $r;
                }
            }
            if (static::$cacheable) {
                $cache->set($cacheKey, $results, 0, $log);
            }
        } else if ($log) {
            ARMREST_Logger::write(get_called_class().'::'.__FUNCTION__.': using cached results; key: '.$cacheKey);
        }
        return new ARMREST_AR_Collection($results);
    }

    public static function getDescendantIds($nodeId, $log=false) {
        $null     = null;
        $useCache = false;
        $cacheKey = get_called_class().'-'.__FUNCTION__.'-'.$nodeId;
        if (static::$cacheable) {
            $cache    = ARMREST_AR_Cache::getInstance();
            $results  = $cache->get($cacheKey, $null, $null, $log);
            if ($cache->getResultCode() !== ARMREST_AR_Cache::RES_NOTFOUND) {
                $useCache = true;
            }
        }
        if (!$useCache) {
            if ($log) ARMREST_Logger::write(get_called_class().'::'.__FUNCTION__.': fetching results; key: '.$cacheKey);
            $results = array();
            $res     = static::getDescendants($nodeId);
            foreach ($res->getCollection() as $r) {
                $results[] = $r->getId();
            }
            if (static::$cacheable) {
                $cache->set($cacheKey, $results, 0, $log);
            }
        } else if ($log) {
            ARMREST_Logger::write(get_called_class().'::'.__FUNCTION__.': using cached results; key: '.$cacheKey);
        }
        return $results;
    }

}


