<?php
/**
 * Defines a collection of objects that extend AR objects for transfer over AJAX
 *
 * @author  Philip Snyder <philip.r.snyder@gmail.com>
 */

/**
 * Wrapper for ARMREST_AR_Interface_toJson objects stored in an array.
 *
 * @todo add support for additional properties at the ARMREST_AR_Collection level, allowing for resultset data (offset, limit, etc) to accompany the results.
 */
class ARMREST_AR_Collection implements ARMREST_AR_Interface_toJson, ArrayAccess, JsonSerializable {

    /**
     * Holds the collection of data internally.
     *
     * @access protected
     * @var    array<AR_Interface_toJson>
     */
    protected $collection = array();

    /**
     * Constructor for collection sets collection array into object.
     *
     * @access public
     * @param  array   $collection
     * @return ARMREST_AR_Collection
     */
    public function __construct(array $collection=null) {
        if (!is_null($collection)) {
            $this->setCollection($collection);
        }
    }

    /*** (( BEGIN ArrayAccess )) ***/

    /**
     * Magic empty() function.
     *
     * @access public
     * @return bool
     */
    public function __empty() {
        return sizeOf($this->collection) > 0;
    }

    /**
     * Magic empty() function.
     *
     * @access public
     * @return bool
     */
    public function offsetExists($offset) {
        return isset($this->collection[$offset]);
    }

    /**
     * Magic function for returning the value stored at the collection's offset / key.
     *
     * @access public
     * @param  mixed   $offset
     * @return mixed
     */
    public function offsetGet($offset) {
        return isset($this->collection[$offset]) ? $this->collection[$offset] : null;
    }

    /**
     * Magic function for setting a key (offset) in the collection.
     *
     * @access public
     * @param  mixed   $offset
     * @param  mixed   $value
     * @return void
     */
    public function offsetSet($offset, $value) {
        $this->collection[$offset] = $value;
    }

    /**
     * Returns the number of items in the collection.
     *
     * @access public
     * @return int
     */
    public function count() {
        return count($this->collection);
    }


    /**
     * Magic function for unsetting a value in the collection by offset key.
     *
     * @access public
     * @param  mixed   $offset
     * @return void
     */
    public function offsetUnset($offset) {
        unset($this->collection[$offset]);
    }

    /*** (( END ArrayAccess )) ***/

    /**
     * Sets the collection into the object.
     * Ensures that the collection only has objects that implement ARMREST_AR_Interface_toJson.
     *
     * @access public
     * @param  array<AR_Interface_toJson>   $collection
     * @return void
     * @throws Exception
     */
    public function setCollection(array $collection=array()) {
        foreach ($collection as $obj) {
            if (!($obj instanceOf ARMREST_AR_Interface_toJson)) {
                throw new Exception('AR_Collection may only contain objects that implement ARMREST_AR_Interface_toJson', E_USER_ERROR);
            }
        }
        $this->collection = $collection;
    }

    /**
     * Returns the collection directly.
     *
     * @access public
     * @return array<AR_Interface_toJson>
     */
    public function getCollection() {
        return $this->collection;
    }

    /*** (( BEGIN ARMREST_AR_Interface_toJson )) ***/

    /**
     * Translates the collection object into a JSON ready object.
     *
     * @param $addlProps   array   Associative array of additional properties to set on the objects in the collection.
     * @since 1.0
     */
    public function toJson() {
        $collection = array();
        foreach ($this->collection as $cObj) {
            $collection[] = $cObj->toJson();
        }
        return $collection;
    }

    /*** (( END ARMREST_AR_Interface_toJson )) ***/

    /**
     * Note that this function does not allow you to set additional properties on the object that wraps the
     * collection inside. It is meant to be used when you need a straight array of ARMREST_AR_Interface_toJson objects
     * directly passed over Ajax and don't need to define extra parameters for it.
     */
    public static function arrayToJson(array $objs) {
        $inst = new self($objs);
        return $inst->toJson();
    }

    public function jsonSerialize() {
        return $this->toJson();
    }

}


?>
