<?php

class ARMREST_AR_Exception extends Exception implements ARMREST_AR_Interface_toJson {

    public function toJson() {
        $ret = array(
            'class'   => 'Exception',
            'message' => $this->getMessage(),
            'code'    => $this->getCode(),
        );
        return $ret;
    }

}
