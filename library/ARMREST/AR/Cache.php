<?php
/**
 * @author Philip Snyder <philip.r.snyder@gmail.com>
 */

/**
 * Cache wrapper for Active Record implementation.
 *
 * @todo implmement via traits ??
 */
class ARMREST_AR_Cache extends Memcached {

    /**
     * Singleton instantiator for cache.
     *
     * @static
     * @access public
     * @@return ARMREST_AR_Cache
     */
    static public function getInstance() {
        static $instance = null;
        if (is_null($instance)) {
            $instance = new static();
        }
        return $instance;
    }

    /**
     * Initializes our cache object to use servers as configured in cache.ini
     *
     * @access public
     * @return ARMREST_AR_Cache
     */
    public function __construct() {
        parent::__construct();
        $servers = $this->getServerList();
        if (empty($servers)) {
            $servers = array();
            $config  = ARMREST_Config::getInstance('cache');
            //error_log('config: '.print_r($config->get('memcached', 'servers'), true));
            foreach ($config->get('memcached', 'servers') as $serverId) {
                $servers[] = $config->get('memcached', $serverId);
            }
            //error_log('servers: '.print_r($servers, true));
            $this->addServers($servers);
         }
    }

    /**
     * Retrieves a value from the cache.
     *
     * @access public
     * @param  string  $key
     * @param  mixed   $cache_cb
     * @param  mixed   $cas_token
     * @param  bool    $log
     * @return mixed
     */
    public function get($key, $cache_cb=null, &$cas_token=null, $log=false) {
        if ($log) ARMREST_Logger::write('cache get on key: '.$key);
        $val = parent::get($key, $cache_cb, $cas_token);
        if ($log) ARMREST_Logger::write('getting key: '.$key.'; value: '.print_r($val, true));
        return $val;
    }

    /**
     * Sets a value in the cache.
     *
     * @access public
     * @param  string  $key
     * @param  mixed   $value
     * @param  int     $expiration
     * @param  bool    $log
     * @return mixed
     */
    public function set($key, $value, $expiration=0, $log=false) {
        if ($log) ARMREST_Logger::write('setting key: '.$key.'; value: '.print_r($value, true));
        return parent::set($key, $value, $expiration);
    }

    /**
     * Deletes a value from the cache.
     *
     * @access public
     * @param  string  $key
     * @param  bool    $log
     * @return mixed
     */
    public function delete($key, $log=false) {
        if ($log) ARMREST_Logger::write('deleting key: '.$key);
        return parent::delete($key);
    }

}

